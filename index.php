<?php
// Подключение классов
require_once 'Category.php';
require_once 'PhoneCategory.php';
require_once 'MonitorCategory.php';

// Создание объектов категорий
$categories = array();

// Пример продуктов
$products = array(
    array(
        'name' => 'iPhone',
        'type' => 'Phone',
    ),
    array(
        'name' => 'Samsung Galaxy',
        'type' => 'Phone',
    ),
    array(
        'name' => 'Dell Monitor',
        'type' => 'Monitor',
    ),
    array(
        'name' => 'LG Monitor',
        'type' => 'Monitor',
    ),
);

// Обработка продуктов
foreach ($products as $product) {
    if ($product['type'] === 'Phone') {
        $category = new PhoneCategory('Phones', array());
        $category->addProduct($product['name']);
        $categories[] = $category;
    } elseif ($product['type'] === 'Monitor') {
        $category = new MonitorCategory('Monitors', array());
        $category->addProduct($product['name']);
        $categories[] = $category;
    }
}

// Вывод результатов
foreach ($categories as $category) {
    echo 'Category: ' . $category->getName() . PHP_EOL;
    echo 'Products: ' . implode(', ', $category->getListProducts()) . PHP_EOL;
    echo 'Filters: ' . implode(', ', $category->getFilters()) . PHP_EOL;
    echo '------------------------' . PHP_EOL;
}
class Category {
    private $name;
    private $filters;
    private $listProducts;
    
    public function __construct($name, $filters) {
        $this->name = $name;
        $this->filters = $filters;
        $this->filters[] = "Price";
        $this->listProducts = array();
    }
}
class PhoneCategory extends Category {
    public function __construct($name, $filters) {
        parent::__construct($name, $filters);
        $this->filters = array_merge($this->filters, ["Ram", "CountSim", "Hdd", "OS"]);
    }
    
    }

  class MonitorCategory extends Category {
    public function __construct($filters){
        parent::_construct($filters);
        $this->filters = array_merge($this->filters, ["Diagonal","Frequency"]);
    }
  }  